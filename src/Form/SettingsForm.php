<?php declare(strict_types = 1);

namespace Drupal\c2pa_sign\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Jrglasgow\C2paTool\Exceptions\RuntimeException;
use Jrglasgow\C2paTool\Tool;

/**
 * Configure C2PA Sign settings for this site.
 */
final class SettingsForm extends ConfigFormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'c2pa_sign_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['c2pa_sign.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    c2pa_sign_check_certificate_expiration();
    $config = $this->config('c2pa_sign.settings');
    $immutable_config = $this->configFactory()->get('c2pa_sign.settings');

    $form['site'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Site Information'),
      '#tree' => TRUE,
    ];
    $form['site']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site/Org Name'),
      '#description' => $this->t('What site or organization name do you want to appear in the manifests when signing? If left blank the <a href="@url" target="_blank">system site name</a> "%site_name" will be used..', [
        '%site_name' => \Drupal::config('system.site')->get('name'),
        '@url' => Url::fromRoute('system.site_information_settings')->toString(),
      ]),
      '#default_value' => $config->get('site')['name'] ?? '',
    ];
    $this->checkOverridden($form['site']['name'], 'site', 'name');

    $form['site']['claim_generator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Claim Generator'),
      '#description' => $this->t('When a manifest is created a claim_generator is added - A User-Agent string formatted as per <a href="http://tools.ietf.org/html/rfc7231#section-5.5.3" target="_blank">http://tools.ietf.org/html/rfc7231#section-5.5.3</a>, for including the name and version of the claims generator that created the claim. If left blank the claim generator for the c2patool library will be used.'),
      '#default_value' => $config->get('site')['claim_generator'] ?? '',
    ];
    $this->checkOverridden($form['site']['claim_generator'], 'site', 'claim_generator');
    $default_logo = c2pa_sign_get_logo();
    $form['site']['logo'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Logo for the claim generator'),
      '#description' => $this->t('This is the logo which will be attached to the manifest for the Claim Generator. You can either have a path relative to the docroot, or if it is in the private or public file system you can use private:// or public://. This will default to the theme logo.'),
      '#default_value' => $default_logo['config'],
      '#suffix' => '<img width="50px" src="' . $default_logo['url'] . '">',
    ];
    $this->checkOverridden($form['site']['logo'], 'site', 'logo');

    $form['certificate'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Certificate'),
    ];
    $form['certificate']['admin_page_load_cert_check'] = [
      '#type' => 'checkbox',
      '#title' => t('Check Certificate on every Admin page load'),
      '#description' => t('When a user with the "administer site configuration" permission loads a page certificates will be checked.'),
      '#default_value' => $config->get('admin_page_load_cert_check') ?? 0,
    ];
    $cert_file_directory = $immutable_config->get('certificate_file_directory');
    $certs = c2pa_sign_get_certs($cert_file_directory);
    // look through the valid certs and determine if one is in the environment variable
    $in_env = FALSE;
    $placeholder = '/path/to/certificate/directory';
    $required = TRUE;
    foreach ($certs['certificates'] AS $this_cert) {
      if ($this_cert->uri == 'ENVIRONMENT_VARIABLE') {
        $placeholder = $this->t('The environment variables contain a valid certificate.');
        $required = FALSE;
      }
    }
    $form['certificate']['certificate_file_directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Certificate file directory'),
      '#description' => $this->t('The directory in which you keep your certificates and keys. When looking to sign media this directory will be searched for valid certificate key pairs. A key pair will consist of 2 files with the same name but different extensions, i.e. cert-@year.pem and cert-@year.key. Only key pairs that are complete will be used. If there are multiple pairs the certificate that expires last will be used. Certificates MUST fulfill all requirements in the <a href="https://opensource.contentauthenticity.org/docs/manifest/signing-manifests/#certificates" target="_blank">documentation</a>.', ['@year' => date('Y')]),
      '#default_value' => $config->get('certificate_file_directory'),
      '#required' => $required,
      '#placeholder' => $placeholder,
    ];
    $this->checkOverridden($form['certificate']['certificate_file_directory'], 'certificate_file_directory');

    if (!empty($certs['cert_to_use']) && is_object($certs['cert_to_use'])) {
      $form['certificate']['cert_info'] = c2pa_sign_certificate_info($certs['cert_to_use']);
    }

    $c2patool_error = FALSE;
    $logger = $this->logger('c2pa_sign');
    $tool = new Tool($logger);
    try {
      if (!empty($immutable_config->get('c2patool_binary_location')) && file_exists($immutable_config->get('c2patool_binary_location')) && is_executable($immutable_config->get('c2patool_binary_location'))) {
        $tool->setBinary($immutable_config->get('c2patool_binary_location'));
      }
    }
    catch (RuntimeException $e) {
      $c2patool_error = $e->getMessage();
      $this->messenger()->addError($this->t('The c2patool binary at @binary_location executed with error: @error', [
        '@error' => $c2patool_error,
        '@binary_location' => $immutable_config->get('c2patool_binary_location'),
      ]));
    }

    $form['c2patool'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('c2patool'),
    ];
    $form['c2patool']['c2patool_binary_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('c2patool location'),
      '#description' => $this->t('The location on the system of the <a href="https://github.com/contentauth/c2patool" target="_blank">c2patool binary which can be found on Github</a>.'),
      '#default_value' => $tool->getBinary(),
    ];
    $this->checkOverridden($form['c2patool']['c2patool_binary_location'], 'c2patool_binary_location');

    if ($tool->getBinary()) {
      $form['c2patool']['c2patool_info'] = [
        '#markup' => '<div class="c2patool-version">' . t('c2patool version: %version.', ['%version' => $tool->getBinaryVersion()]) . '</div>',
      ];
    }

    $form['assertions'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Assertions'),
      '#description' => $this->t('What actions should trigger assertions being added to media assets?'),
      '#tree' => TRUE,
    ];

    $form['assertions']['upload'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Assert that files were uploaded to the site.'),
      '#description' => $this->t('Integration with the FileWidget will cause assertions to be added upon upload.'),
      '#default_value' => $config->get('assertions')['upload'] ?? TRUE,
    ];
    $this->checkOverridden( $form['assertions']['upload'], 'assertions', 'upload');

    $form['assertions']['publish'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Assert that files were published.'),
      '#description' => $this->t('Integration with the Node module will cause assertions to be added to files upon publication.'),
      '#default_value' => $config->get('assertions')['publish'] ?? TRUE,
    ];
    $this->checkOverridden( $form['assertions']['publish'], 'assertions', 'publish');

    $form['image_derivatives'] = [
      '#type' => 'details',
      '#title' => $this->t('Image Derivatives'),
      '#description' => $this->t('Image that are based on other images (thumbnails, medium/large, etc...)'),
      '#tree' => TRUE,
    ];
    $form['image_derivatives']['add_manifests'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add Manifests'),
      '#description' => $this->t('Should manifests declaring the derivative and change made by the derivative be added to derivative images?'),
      '#default_value' => $config->get('image_derivatives')['add_manifests'] ?? TRUE,
    ];
    $form['image_derivatives']['add_when_original_does_not_have_manifest'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add even though the original doesn\'t have a manifest?'),
      '#description' => $this->t('If the original image doesn\'t have a manifest should a manifest be added to the derivative?'),
      '#default_value' => $config->get('image_derivatives')['add_when_original_does_not_have_manifest'] ?? FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $cert_directory_config = $form_state->getValue('certificate_file_directory');
    $immutable_config = $this->configFactory()->get('c2pa_sign.settings');
    $is_overridden = $immutable_config->hasOverrides('certificate_file_directory');
    if ($is_overridden) {
      $cert_directory_config = $immutable_config->get('certificate_file_directory');
    }
    if (!is_dir($cert_directory_config)) {
      $form_state->setErrorByName('certificate_file_directory', $this->t('The certificate files directory %directory does not exist.', ['%directory' => $form_state->getValue('certificate_file_directory')]));
    }
    else {
      $certs = c2pa_sign_get_certs($form_state->getValue('certificate_file_directory'));
      if (empty($certs['cert_to_use'])) {
        $form_state->setErrorByName('certificate_file_directory', $this->t('No valid cert/key pairs in the directory %directory.', ['%directory' => $form_state->getValue('certificate_file_directory')]));

      }
      if (!empty($certs['errors'])) {
        foreach ($certs['errors'] AS $error) {
          if (isset($error['text']) && !empty($error['text'])) {
            $form_state->setErrorByName('certificate_file_directory', $error['text'], $error['args'] ?? []);
            $this->messenger()->addWarning($error['text'], $error['args'] ?? []);
          }
        }
      }
    }

    // validate c2patool binary
    $binary_location = $form_state->getValue('c2patool_binary_location');
    if (empty($binary_location)) {
      $this->messenger()->addWarning('c2patool binary location is empty.');
      $this->searchBinary($form_state);
      $this->messenger()->addWarning('c2patool binary location is empty.');
      if (empty($form_state->getValue('c2patool_binary_location'))) {
        $form_state->setErrorByName('c2patool_binary_location', $this->t('c2patool binary location is required.'));
      }
    }
    else if (!file_exists($binary_location)) {
      $this->messenger()->addWarning($this->t('c2patool binary location (%location) does not exist.', ['%location' => $binary_location]));
      $this->searchBinary($form_state);
      if ($binary_location == $form_state->getValue('c2patool_binary_location')) {
        // the binary location didn't change
        $form_state->setErrorByName('c2patool_binary_location', $this->t('c2patool binary location is required and MUST exist.'));
      }
    }
    else if (!is_executable($binary_location)) {
      $this->messenger()->addWarning($this->t('c2patool binary location (%location) is not executable.', ['%location' => $binary_location]));
      $this->searchBinary($form_state);
      if ($binary_location == $form_state->getValue('c2patool_binary_location')) {
        // the binary location didn't change
        $form_state->setErrorByName('c2patool_binary_location', $this->t('c2patool binary location is required and MUST be executable.'));
      }
    }
    else {
      // the location is not empty, the file does exist and it is executable
      // see if we can get a valid version
      $c2patool_error = FALSE;
      $logger = $this->logger('c2pa_sign');
      $tool = new Tool($logger);
      try {
        $tool->setBinary($binary_location);
        $version = $tool->getBinaryVersion();
      }
      catch (RuntimeException $e) {
        $c2patool_error = $e->getMessage();
        $form_state->setErrorByName('c2patool_binary_location', $this->t('Could not get version for c2patool. @error', ['@error' => $c2patool_error]));
      }
    }


    parent::validateForm($form, $form_state);
  }

  /**
   * Search for the c2patool binary ans set it if found
   *
   * @param $form_state
   *
   * @return void
   */
  protected function searchBinary($form_state) {
    $tool = new Tool($this->logger('c2pa_sign'));
    $binary_location = $tool->getBinary();
    if (!empty($binary_location)) {
      $form_state->setValue('c2patool_binary_location', $binary_location);
      \Drupal::messenger()->addStatus('c2patool binary found at %location', ['%location' => $binary_location]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('c2pa_sign.settings');
    // remove extraneous form values
    $form_state->cleanValues();
    // set all form values
    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $value);
    }
    // save the config
    $config->save();
    parent::submitForm($form, $form_state);
  }

  protected function checkOverridden(&$element, $key, $subkey = NULL) {
    $immutable_config = $this->configFactory()->get('c2pa_sign.settings');
    if (!$immutable_config->hasOverrides($key)) {
      return;
    }
    $overridden_value = $immutable_config->get($key);
    if (is_array($overridden_value) && !empty($subkey) && isset($overridden_value[$subkey])) {
      $overridden_value = $overridden_value[$subkey];
    }

    # modify the element to let the end user know there are overrides
    $element['#disabled'] = TRUE;
    // if it is overridden then it dones't need to be required
    $element['#required'] = FALSE;
    $element['#suffix'] = $this->t('The value is being overridden with "%value".', ['%value' => $overridden_value]);
  }

}
