<?php declare(strict_types = 1);

namespace Drupal\c2pa_sign\EventSubscriber;

use Drupal\c2pa_sign\Event\PreSignEvent;
use Drupal\Component\Datetime\Time;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\ProxyClass\File\MimeType\MimeTypeGuesser;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Jrglasgow\C2paTool\Signer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * @todo Add description for this subscriber.
 */
final class C2paSignSubscriber implements EventSubscriberInterface {
  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\ProxyClass\File\MimeType\MimeTypeGuesser
   */
  protected MimeTypeGuesser $fileMimeTypeGuesser;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routematch;

  /**
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapperManager;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $current_user;

  /**
   * @var \Drupal\Component\Datetime\Time
   */
  protected Time $time;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The configuration for the module
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected \Drupal\Core\Config\ImmutableConfig $config;


  /**
   * Constructs a C2paSignSubscriber object.
   */
  public function __construct(MimeTypeGuesser $fileMimeTypeGuesser, FileSystemInterface $fileSystem, RouteMatchInterface $routematch, StreamWrapperManagerInterface $streamWrapperManager, ConfigFactoryInterface $configFactory, MessengerInterface $messenger, AccountProxyInterface $current_user, Time $time, DateFormatterInterface $date_formatter
  ) {
    $this->fileMimeTypeGuesser = $fileMimeTypeGuesser;
    $this->fileSystem = $fileSystem;
    $this->routematch = $routematch;
    $this->streamWrapperManager = $streamWrapperManager;
    $this->configFactory = $configFactory;
    $this->messenger = $messenger;
    $this->current_user = $current_user;
    $this->time = $time;
    $this->dateFormatter = $date_formatter;
    $this->config = $this->configFactory->get('c2pa_sign.settings');
  }

  /**
   * Kernel request event handler.
   */
  public function onKernelRequest(RequestEvent $event): void {
    $this->checkCertificateExpiration();

  }

  /**
   * In some instances the request has no route, when this is the case the route
   * and other information can be inferred, try to infer it.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *
   * @return array
   */
  protected function emptyRoute(ResponseEvent $event) {
    $image_style_name = NULL;
    $image_style = NULL;
    $route = NULL;
    $request = $event->getRequest();
    $path_info = $request->getPathInfo();
    $directory_path = $this->streamWrapperManager->getViaScheme('public')->getDirectoryPath();
    // get the position (if it exists) for the public scheme directory
    $path_position = strpos($path_info, '/' . $directory_path . '/styles');
    $scheme = 'public';
    if ($path_position === 0) {
      $route = 'image.style_public';
      $path_info_exploded = explode('/' . $directory_path . '/styles/', $path_info);
      [$image_style_name] = explode('/', $path_info_exploded[1]);
      $image_style = ImageStyle::load($image_style_name);
    }
    // remove any styles parts of the path
    $target_path_replace = '/' . $directory_path . '/styles/' . $image_style_name . '/' . $scheme . '/';
    $target = str_replace($target_path_replace, '', $path_info);

    return [$route, $scheme, $image_style, $target];
  }

  /**
   * removes extra extensions until we find a valid target file, or nothing
   *
   * @param (string) $target
   *
   * @return null
   */
  protected function verifyTargetFile(string $target) {
    if (!str_contains('public://', $target)) {
      $target_to_test = 'public://' . $target;
    }
    else {
      $target_to_test = $target;
    }
    if (file_exists($target_to_test)) {
      return $target;
    }
    if (strpos($target, '.' )) {
      $possible_target = explode('.', $target);
      // pop off any extra extension
      $ext = array_pop($possible_target);
      return $this->verifyTargetFile(implode('.', $possible_target));
    }
    return NULL;
  }

  /**
   * Kernel response event handler.
   */
  public function onKernelResponse(ResponseEvent $event): void {

    $route = $this->routematch->getRouteName();
    $request = $event->getRequest();

    if (empty($route)) {
      // just to make sure this isn't a file in the public image style as
      // drupal/webp causes different effects
      [$route, $scheme, $image_style, $target] = $this->emptyRoute($event);

    }
    switch ($route) {
      case 'image.style_public':
      case 'image.style_private':
        break;
      default:
        // we don't want these routes - skip it
        return;
    }

    if (empty($this->config->get('image_derivatives')) || !$this->config->get('image_derivatives')['add_manifests']) {
      // configuration is to not add manifests to image derivatives
      return;
    }

    //get the target file
    $target = $request->query->get('file') ?? $target;
    $target = $this->verifyTargetFile($target);

    if (empty($target)) {
      // no target file, do nothing
      return;
    }

    // get the scheme
    $scheme = $request->attributes->get('scheme') ?? $scheme;

    // get the image style, if it is null use one we might already have
    $image_style = $request->attributes->get('image_style') ?? $image_style;
    if (empty($image_style)) {
      // we have no image style, do nothing
      return;
    }

    // create the original image uri
    $image_uri = $scheme . '://' . $target;
    // normalize it
    $image_uri = $this->streamWrapperManager->normalizeUri($image_uri);

    // generate the derivative image uri
    $derivative_uri = $image_style->buildUri($image_uri);
    // and get path info
    $derivative_pathinfo = pathinfo($derivative_uri);
    // and mime type
    $derivative_mime_type = $this->fileMimeTypeGuesser->guessMimeType($derivative_uri);

    if (Signer::mimeTypeAllowed($derivative_mime_type) || Signer::fileExtensionAllowed($derivative_pathinfo['extension'])) {
      $source_path = $this->fileSystem->realpath($image_uri);

      $derivative_path = $this->fileSystem->realpath($derivative_uri);

      // create a temporary fisle
      $file = File::create([
        'filename' => basename($derivative_uri),
        'uri' => '$derivative_uri',
        'status' => 1,
        'uid' => 1,
      ]);

      $manifest = c2pa_sign_create_manifest($file, [
        'operation' => 'image_derivative',
        'entity' => $file,
        'image_style' => $image_style,
        'assertions' => [
          [
            'label'=> 'drupal.c2pa_sign.image_derivative', // a custom assertion from this module
            'data'=> [
              'generation'=> strtr('Image derivative generated for file @image_name in style @style_name at site @site_name.', [
                '@site_name' => $this->config->get('site')['name'] ?? \Drupal::config('system.site')->get('name'),
                '@image_name' => basename($image_uri),
                '@style_name' => $image_style->label(),
              ]),
            ],
          ],
        ],
      ]);

      // we can attempt to sign the file
      $signer = c2pa_sign_get_signer();
      if (!$this->config->get('image_derivatives')['add_when_original_does_not_have_manifest']) {
        // we need to check to see if the original has a manifest, otherwise return
        $tool = $signer->getTool();
        try{
          $current_manifests = $tool->checkManifest($source_path);
        }
        catch (\Jrglasgow\C2paTool\Exceptions\RuntimeException $e) {
          if (str_contains($e->getMessage(), 'No claim found')) {
            // the source image doesn't have a claim, so we skip adding one to
            // the derivative
            return;
          }
        }
        if (empty($current_manifests)) {
          // the original didn't have a manifest, don't add one for the derivative
          return;
        }

      }
      // We are specifically defining the source and destination file, the
      // original "source" image that this derivative was created from is being
      // used as the parent

      $result = $signer->sign($derivative_path, $derivative_path, $manifest, $source_path);
      if ($result) {
        // only record the usa of the certificate if the signing occurred
        c2pa_sign_record_certificate_usage($signer);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::REQUEST => ['onKernelRequest'],
      KernelEvents::RESPONSE => ['onKernelResponse', -1000],
    ];
  }

  /**
   * notify admin users if the certificate is expired, or will expire shortly
   *
   * @return void
   */
  private function checkCertificateExpiration(): void {
    if (!$this->config->get('admin_page_load_cert_check')) {
      // don't check
      return;
    }
    c2pa_sign_check_certificate_expiration();
  }

}
