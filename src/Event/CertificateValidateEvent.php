<?php

namespace Drupal\c2pa_sign\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Wraps an indexing items event.
 */
final class CertificateValidateEvent extends Event {

  const EVENT_NAME = 'c2pa_sign_certificate_validate_event';

  /**
   * @var \stdClass
   */
  protected $certFile;

  /**
   * @var boolean
   */
  protected bool $isValid = FALSE;

  /**
   * Creates the new class instance
   *
   * @param \stdClass $certFile The cert file object to be validated
   */
  public function __construct(\stdClass &$certFile) {
    $this->certFile = $certFile;
  }

  /**
   * returns the certificate that (is to be/was) validated
   */
  public function getCertificate(): \stdClass {
    return $this->certFile;
  }

  /**
   * set the certificate data
   *
   * @param $cert_file
   *
   * @return void
   */
  public function setCertificate(\stdClass $certFile) {
    $this->certFile = $certFile;
  }

  /**
   * sets the isValid property
   *
   * @param $setting
   *
   * @return bool
   */
  public function isValid($setting = NULL) {
    if (!is_null($setting)) {
      $this->isValid = (bool) $setting;
    }
    return $this->isValid;
  }

}
