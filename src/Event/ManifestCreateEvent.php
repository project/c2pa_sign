<?php

namespace Drupal\c2pa_sign\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\file\FileInterface;

/**
 * Wraps a manifest creation event. This allows other modules to alter the
 * manifest which gets embedded.
 */
final class ManifestCreateEvent extends Event {

  const EVENT_NAME = 'c2pa_sign_manifest_create_event';

  /**
   * The manifest to be altered
   *
   * @var array
   */
  protected array $manifest;

  /**
   * The file the manifest is being created for
   *
   * @var \Drupal\file\FileInterface
   */
  protected FileInterface $file;

  /**
   * The entity being acted upon
   */
  protected ContentEntityInterface $entity;

  protected array $preventManifestEmbedReasons = [];

  /**
   * Creates the new class instance
   *
   * @param array $manifest The manifest array to be passed to c2patool as JSON
   */
  public function __construct(array &$manifest, FileInterface $file) {
    $this->manifest = $manifest;
    $this->file = $file;
  }

  /**
   * Set the Entity to be attached to the event.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return void
   */
  public function setEntity(ContentEntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Get the Entity attached to the event
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|false
   */
  public function getEntity() {
    return $this->entity ?? FALSE;
  }

  /**
   * Does the event have an attached Entity?
   *
   * @return bool
   */
  public function hasEntity() {
    return isset($this->entity);
  }

  /**
   * Retrieves the potentially altered manifest
   */
  public function getManifest(): array {
    return $this->manifest;
  }

  /**
   * set the manifest data
   *
   * @param array $manifest
   *
   * @return void
   */
  public function setManifest(array $manifest) {
    $this->manifest = $manifest;
  }

  /**
   * Add a reason why the manifest shouldn't be embedded.
   *
   * @param string $reason
   *
   * @return void
   */
  public function addPreventManifestEmbedReason(string $reason) {
    $this->preventManifestEmbedReasons[] = $reason;
  }

  /**
   * Return the reasons why the manifest isn't getting embedded.
   *
   * @return array|\Drupal\c2pa_sign\Event\ManifestCreateEvent
   */
  public function getPreventManifestEmbedReasons() {
    return $this->preventManifestEmbedReasons;
  }

  /**
   * Can a manifest be embedded, or is it prevented for some reason?
   *
   * @return bool
   */
  public function isManifestEmbeddingPrevented() {
    return (bool) count($this->preventManifestEmbedReasons);
  }

}
