<?php

namespace Drupal\c2pa_sign\Event;

use Drupal\Component\EventDispatcher\Event;
use Jrglasgow\C2paTool\Signer;

/**
 * Wraps an presign event. This allows other modules to alter the signer
 * configuration.
 */
final class PreSignEvent extends Event {

  const EVENT_NAME = 'c2pa_sign_pre_sign_event';

  /**
   * @var \Jrglasgow\C2paTool\Signer
   */
  private Signer $signer;

  /**
   * Creates the new class instance
   *
   * @param Signer $signer The Signer object to do the signing
   */
  public function __construct(Signer $signer) {
    $this->signer = $signer;
  }

  /**
   * Retrieves the potentially altered Signer
   */
  public function getSigner(): Signer {
    return $this->signer;
  }

  /**
   * set the Signer object
   *
   * @param Signer $signer
   *
   * @return void
   */
  public function setSigner(Signer $signer) {
    $this->signer = $signer;
  }

}
