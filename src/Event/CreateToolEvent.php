<?php

namespace Drupal\c2pa_sign\Event;

use Drupal\Component\EventDispatcher\Event;
use Jrglasgow\C2paTool\Tool;

/**
 * Wraps an indexing items event.
 */
final class CreateToolEvent extends Event {

  const EVENT_NAME = 'c2pa_sign_create_tool_event';

  /**
   * @var \Jrglasgow\C2paTool\Tool
   */
  private Tool $tool;

  /**
   * Creates the new class instance
   *
   * @param Tool $tool The Tool objct to execute c2patool commands
   */
  public function __construct(Tool $tool) {
    $this->tool = $tool;
  }

  /**
   * Retrieves the potentially altered Tool object
   */
  public function getTool(): Tool {
    return $this->tool;
  }

  /**
   * set the tool object
   *
   * @param array $tool
   *
   * @return void
   */
  public function setTool(Tool $tool) {
    $this->tool = $tool;
  }

}
