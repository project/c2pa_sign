## INTRODUCTION

The C2PA Sign module is designed to integreate ith the [c2patool wrapper library](https://github.com/jrglasgow/c2patool)  which used the [c2patool command line utility](https://github.com/contentauth/c2patool).

The primary use case for this module is:

- Digitally signing manifests for media files that are uploaded to the site.

The module will sign:
- media files uploaded to the site
- media files attached to public content entities
- derivative images (image styles) upon creation

## REQUIREMENTS

The [c2patool wrapper library](https://github.com/jrglasgow/c2patool). The [c2patool executable](https://github.com/contentauth/c2patool).

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

Ensure that the c2patool binary is on the system

## CONFIGURATION
- Go to /admin/config/media/c2pa_sign
- Enter in the path to your certificates directory, if the certificate and key are in the $C2PA_SIGN_CERT and $C2PA_PRIVATE_KEY environment variables they will be automatically detected.
- Enter in the path to the c2patool binary (if it is located in the $PATH it will be automatically detected).

## MAINTAINERS

Current maintainers for Drupal 10:

- James Glasgow (jrglasgow) - https://www.drupal.org/u/jrglasgow
